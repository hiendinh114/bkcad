const http = require('http');

const server = http.createServer((req, res) => {
  res.writeHead(200, {'Content-Type': 'text/plain'});
  res.end(`Hello, World! - Created time: ${process.env.TIME}\n`);
});

const port = process.env.APP_PORT || 8080;
const host = '0.0.0.0';

server.listen(port, host, () => {
  console.log(`Server is running on http://${host}:${port} in ${process.env.APP_ENV} environment`);
});

